import { DATA_ACTION_TYPES } from './data.types';

const { START_GAME, GENERATE_BOARD, SELECT_SLOT, SCORE_CHECKING, IS_GAME_OVER } = DATA_ACTION_TYPES;

const STATE = {
    gameStarted : false,
    board       : [], 
    gameOver    : false,
    move        : 0,
    scoreArray  : [],
    score       : 0
};

const DataReducer = (state = STATE, action) => {

    const { type, payload } = action;

    let mappedArray = [];
    let scoring = 0;
    let scoreChecker = [];

    switch (type) {
        case START_GAME:
            return { ...state, gameStarted: payload };

        case GENERATE_BOARD:
            return { ...state, board: payload };

        case SELECT_SLOT:
            mappedArray = state.board.map((slot, index) => {
                if (index === payload) { return { ...slot, isSelected: true, moveNum: (state.move + 1) } } else return slot;
            });
            return { ...state, board: mappedArray, move: (state.move + 1) };

        case SCORE_CHECKING:

            state.board.forEach(slot => {
                if (slot.isSelected && !slot.didItScore) { 
                    scoreChecker.push(slot);
                    scoreChecker.sort((a, b) => a.moveNum - b.moveNum);
                };
            });
            
            const slot_1 = scoreChecker.length > 0 && scoreChecker[0];
            const slot_2 = scoreChecker.length > 0 && scoreChecker[1];

            if (scoreChecker.length === 2) {
                if (slot_2.img === slot_1.img) {                             
                    scoring = state.score + 5;
                    mappedArray = state.board.map(slot => {
                        if (slot.img === slot_2.img) { 
                            return { ...slot, didItScore: true };
                        } else return slot;
                    });
                    scoreChecker = [];
                    return { ...state, board: mappedArray, score: scoring };
                } else {
                    mappedArray = state.board.map(slot => { 
                        if (slot.didItScore) {
                            return slot;
                        } else return { ...slot, didItScore: false, isSelected: false };
                    });
                    return { ...state, board: mappedArray };
                }
            }

        case IS_GAME_OVER:
            return { ...state, gameOver: payload };
        
        default:
            return state;
    }
};

export default DataReducer;