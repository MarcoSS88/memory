export const startGame = (data) => createAction(DATA_ACTION_TYPES.START_GAME, data);

export const generateBoard = (data) => createAction(DATA_ACTION_TYPES.GENERATE_BOARD, data);

export const selectSlot = (data) => createAction(DATA_ACTION_TYPES.SELECT_SLOT, data);

export const scoreChecking = (data) => createAction(DATA_ACTION_TYPES.MANIPULATE_SCORE_ARRAY, data);

export const finishGame = (data) => createAction(DATA_ACTION_TYPES.IS_GAME_OVER, data);