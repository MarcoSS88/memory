import React, { useEffect, useState } from 'react';
import { BoardGame } from './board.style';

import { useDispatch, useSelector } from "react-redux";
import { DATA_ACTION_TYPES } from '../../store/data/data.types';

import Slot from '../slot/slot.component';
import images from '../../utils/imgs.array';
import { shuffle } from 'lodash';

const Board = () => {

    const data = useSelector((state) => state.data);
    const { board } = data;

    const dispatch = useDispatch();

    const [imgs] = useState(shuffle([...images, ...images]));

    useEffect(() => { 
        const { GENERATE_BOARD } = DATA_ACTION_TYPES;
        let newBoard = imgs.map((image, index) => { return { isSelected: false, id: index, img: image, didItScore: false }});
        dispatch({ type: GENERATE_BOARD, payload: newBoard });
    }, [dispatch, imgs]);


    
    return (
        <BoardGame>
            {board?.map(slot => <Slot key={slot.id} slot={slot}/> )}
        </BoardGame>
    );
}

export default Board;