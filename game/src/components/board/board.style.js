import styled from 'styled-components';

export const BoardGame = styled.div`
  display: grid;
  grid-template-columns: auto auto auto auto;
  column-gap: 0.1rem;
  row-gap: 0.3rem;
  padding: 0 0 0 1rem;
  width: 35rem;
  height: 32rem;
`;