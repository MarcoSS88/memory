import React from 'react';
import { TopDiv, MainTitle, TitleContainer, TimerContainer } from './titleSection.style';

import Timer from '../timer/timer.component';
import { useSelector } from 'react-redux';

const TitleSection = () => {

    const data = useSelector((state) => state.data);
    const { gameStarted } = data;

    return (
        <TopDiv>

            <TitleContainer>
                <MainTitle>Memory</MainTitle>
            </TitleContainer>

            {gameStarted && <TimerContainer> <Timer /> </TimerContainer>}

        </TopDiv>
    );
}

export default TitleSection;