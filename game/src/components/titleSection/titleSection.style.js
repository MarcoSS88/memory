import styled from 'styled-components';

export const TopDiv = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  height: auto;
`;

export const TitleContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-end;
  width: 75%;
  height: 100%;
  padding-right: 3.5%;
`;

export const TimerContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-start;
  width: 25%;
  height: 100%;
`;

export const MainTitle = styled.h1`
  font-family: 'Monoton', cursive;
  font-size: 6.5rem;
  color: #e7c6ff;
`;