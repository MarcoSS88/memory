import React from 'react';
import { Image, Pic } from './picture.style';

const Picture = ({ img }) => {

    return (
        <Image>
            <Pic src={img} alt="picture" />
        </Image>
    );
}

export default Picture;