import styled from 'styled-components';

export const Image = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 7.5rem;
  height: 7.5rem;
  border-radius: 0.3rem;
  background-color: #274B68;
  cursor: pointer;
`;

export const Pic = styled.img`
  width: 100%;
  height: 100%;
  border-radius: 0.3rem;
`;
