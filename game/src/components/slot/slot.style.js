import styled from 'styled-components';

export const Box = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 7.5rem;
  height: 7.5rem;
  border-radius: 0.3rem;
  background-color: #274B68;
  cursor: pointer;
  border: none;

  :active {
    transition: transform ease;
    transform: rotateY(180deg);
    transform-style: preserve-3d;
    animation: showImage 0.8s;

    @keyframes showImage {
    0%, 50% {
      transform: rotateY(0deg);
    }
    100% {
      transform: rotateY(360deg);
    }
  }
  }
`;

   