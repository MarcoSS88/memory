import React, { Fragment, useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { DATA_ACTION_TYPES } from '../../store/data/data.types';

import { Box } from './slot.style';
import Image from '../picture/picture.component';

const Slot = ({ slot }) => {

    const { id, isSelected, img, didItScore } = slot;

    const dispatch = useDispatch();

    const [counter, setCounter] = useState(0);
    const [checkingSlots, setCheckingSlots] = useState(false);

    const selectSlot = (i) => {
        const { SELECT_SLOT } = DATA_ACTION_TYPES;
        dispatch({ type: SELECT_SLOT, payload: i});
        setCounter(counter + 1);
    };

    useEffect(() => {
        if (counter === 2) {
            setCheckingSlots(true);
            setCounter(0);
        }
    }, [counter]);
    useEffect(() => {
        setInterval(() => {
            const { SCORE_CHECKING } = DATA_ACTION_TYPES;
            dispatch({ type: SCORE_CHECKING });
        }, 3000);
    }, [checkingSlots]);

    const handleVisibility = () => {
        return didItScore && "none";
    };

    return (
        <Fragment>
            { !isSelected ? <Box onClick={() => selectSlot(id)} /> : <Image style={{ display: { handleVisibility } }} img={img} /> }
        </Fragment>
    );
} 

export default Slot;