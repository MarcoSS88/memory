import styled from 'styled-components';

export const CountDown = styled.div`
    margin-left: 14.2%;
`;

export const Time = styled.p`
    font-family: 'Pacifico', cursive;
    font-size: 1.85rem;
    color: white;
`;

export const Message = styled.h3`
    font-family: 'Pacifico', cursive;
    font-size: 1.85rem;
    color: white;
`;