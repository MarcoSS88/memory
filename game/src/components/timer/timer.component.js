import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';

import { CountDown, Time, Message } from './timer.style';
import { DATA_ACTION_TYPES } from '../../store/data/data.types';

const Timer = () => {

    const dispatch = useDispatch();

    const [seconds, setSeconds] = useState(60); 

    useEffect(() => {
        const interval = setInterval(() => {
            setSeconds(seconds - 1);
        }, 1000);
        return () => clearInterval(interval);
    }, [seconds, setSeconds]);

    useEffect(() => {
        const { IS_GAME_OVER } = DATA_ACTION_TYPES;

        if (seconds === 0) {
            dispatch({ type: IS_GAME_OVER, payload: true });
        }
    }, [seconds, dispatch]);

    return (
        <CountDown>
            { seconds >= 0 && <Time> {seconds} sec </Time> }
            { seconds < 0 && <Message> Game Over!! </Message> } 
        </CountDown>
    );
}

export default Timer;