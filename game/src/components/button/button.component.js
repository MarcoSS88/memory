import React from 'react';
import { useDispatch } from 'react-redux';
import { DATA_ACTION_TYPES } from '../../store/data/data.types';
import { BaseButton } from './button.style';

const Button = () => {

  const dispatch = useDispatch();

  const startPlaying = () => {
    const { START_GAME } = DATA_ACTION_TYPES;
    dispatch({ type: START_GAME, payload: true });
  };

  return (
    <BaseButton onClick={() => startPlaying()}> Start Game </BaseButton>
  );
}

export default Button;