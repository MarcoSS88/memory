import styled from 'styled-components'

export const BaseButton = styled.button`
    width: 40%;
    height: 12%;
    border: none;
    border-radius: 0.5rem;
    cursor: pointer;
    font-family: 'Pacifico', cursive;
    font-size: 2.3rem;
    color: #282C33;
    background-color: #fcf6bd;
`;