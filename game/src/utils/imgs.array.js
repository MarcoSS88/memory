import img1 from './images/bocelli.jpeg';
import img2 from './images/de_sica.png';
import img3 from './images/hegel.jpeg';
import img4 from './images/junior.jpeg';
import img5 from './images/kant.webp';
import img6 from './images/masala.jpeg';
import img7 from './images/quadrifoglio.jpeg';
import img8 from './images/stevie_wonder.jpeg';

let arrayImgs = [img1, img2, img3, img4, img5, img6, img7, img8];

export default arrayImgs;