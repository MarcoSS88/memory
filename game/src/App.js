import React from 'react';
import styled from 'styled-components';

import Board from './components/board/board.component';
import TitleSection from './components/titleSection/titleSection.component';

import { useSelector } from "react-redux";
import Button from './components/button/button.component';

const App = () => {

  const data = useSelector((state) => state.data);
  const { gameStarted } = data;

  return (
    <GlobalContainer>
      <TitleSection/>
      { !gameStarted ? <Button/> : <Board/> }
    </GlobalContainer>
  );
}

export default App;

const GlobalContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  width: 100vw;
  height: 100vh;
  background-color: #282C33;
`;